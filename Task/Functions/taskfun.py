# -*- coding: utf-8 -*-
# @Time    : 2020/6/3
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : taskfun.py
from .. import models


def get_task_list(user_get):
    if user_get.is_superuser:
        list_get = models.Task.objects.all()
    else:
        list_get = models.Task.objects.filter(user=user_get)
    return list_get


def check_task_permission(task_id, user_get):
    if user_get.is_superuser:
        item_get = models.Task.objects.filter(id=task_id).first()
    else:
        item_get = models.Task.objects.filter(user=user_get, id=task_id).first()
    return item_get