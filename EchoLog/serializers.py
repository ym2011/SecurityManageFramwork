# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py

from rest_framework import serializers
from . import models


class EchoLogSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.EchoLog
        fields = "__all__"
