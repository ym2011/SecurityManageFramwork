# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models
from django.contrib.auth.models import User


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Role
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    person = serializers.SerializerMethodField('get_person')
    title = serializers.CharField(source='profile.title')

    class Meta:
        model = models.User
        fields = ('username', 'is_superuser', 'title', 'person')

    def get_person(self, obj):
        if obj.profile.person:
            return {'id': obj.profile.person.id, 'name': obj.profile.person.name}
        return None


class UserListSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='profile.title')
    person = serializers.SerializerMethodField('get_person')
    roles = serializers.SerializerMethodField('get_roles')
    permissionmanage = serializers.SerializerMethodField('get_permissionmanage')

    class Meta:
        model = User
        fields = ('id', 'username', 'is_active', 'title', 'person', 'roles', 'permissionmanage')

    def get_person(self, obj):
        if obj.profile.person:
            return {'id': obj.profile.person.id, 'name': obj.profile.person.name}
        return None

    def get_roles(self, obj):
        if obj.profile.roles:
            return {'id': obj.profile.roles.id, 'name': obj.profile.roles.name}
        return None

    def get_permissionmanage(self, obj):
        permissionmanage_list = []
        for item in obj.profile.permissionmanage.all():
            permissionmanage_list.append({'id': item.id, 'name': item.name})
        return permissionmanage_list
