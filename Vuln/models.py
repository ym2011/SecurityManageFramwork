# coding:utf-8
from django.db import models
from Asset.models import Asset
from django.contrib.auth.models import User
from Task.models import Task


# Create your models here.
class LEVEL(models.Model):
    name = models.CharField('风险等级', max_length=100)
    key = models.CharField('等级标记', max_length=10)
    descriptions = models.TextField('等级描述')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'LEVEL'
        verbose_name_plural = '风险等级'


class Type(models.Model):
    name = models.CharField('漏洞类型', max_length=50)
    descriptions = models.TextField('漏洞描述')
    fix = models.TextField('常规建议', null=True)

    def __str__(self):
        # 显示层级菜单
        return self.name

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = '漏洞分类'


class Source(models.Model):
    name = models.CharField('漏洞来源', max_length=50)
    key = models.CharField('来源标识', max_length=10)
    descriptions = models.TextField('来源描述')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Source'
        verbose_name_plural = '漏洞来源'


class AdvanceVuln(models.Model):
    name = models.CharField('漏洞名称', max_length=50)
    type = models.ForeignKey(Type, verbose_name='漏洞分类', related_name='type_for_advancevuln', null=True, blank=True,
                             on_delete=models.SET_NULL)
    level = models.ForeignKey(LEVEL, verbose_name='漏洞分级', related_name='level_for_advancevuln', null=True, blank=True,
                              on_delete=models.SET_NULL)
    source = models.ForeignKey(Source, verbose_name='漏洞来源', related_name='source_for_advancevuln', null=True, blank=True,
                               on_delete=models.SET_NULL)

    introduce = models.TextField('漏洞简介', null=True)
    fix = models.TextField('修复方案', null=True)

    create_time = models.DateTimeField('添加时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'AdvanceVuln'
        verbose_name_plural = '漏洞过滤'


# 用来记录各种来源与资产相关的漏洞
class Vuln(models.Model):
    name = models.CharField('漏洞名称', max_length=50)
    type = models.ForeignKey(Type, verbose_name='漏洞分类', related_name='type_for_vuln', null=True, blank=True,
                             on_delete=models.SET_NULL)
    level = models.ForeignKey(LEVEL, verbose_name='漏洞分级', related_name='level_for_vuln', null=True, blank=True,
                              on_delete=models.SET_NULL)
    source = models.ForeignKey(Source, verbose_name='漏洞来源', related_name='source_for_vuln', null=True, blank=True,
                               on_delete=models.SET_NULL)

    is_check = models.BooleanField('是否有效', default=False)
    introduce = models.TextField('漏洞简介', null=True)
    info = models.TextField('漏洞信息', null=True)
    scope = models.TextField('影响范围', null=True, blank=False)
    fix = models.TextField('修复方案', null=True)
    status = models.CharField('漏洞状态', max_length=50)

    create_time = models.DateTimeField('发现时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    user = models.ForeignKey(User,related_name='user_for_vuln', on_delete=models.SET_NULL, null=True)
    asset = models.ForeignKey(Asset, related_name='vuln_for_asset', on_delete=models.CASCADE, null=True)
    task = models.ForeignKey(Task, related_name='task_for_vuln', on_delete=models.SET_NULL,null=True, blank=True)
    advance_vuln = models.ForeignKey(AdvanceVuln, related_name='advance_for_vuln', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Vuln'
        verbose_name_plural = '资产漏洞'


