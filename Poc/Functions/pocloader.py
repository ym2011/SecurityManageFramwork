# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : pocload.py

from SemfPro.settings import POC_PATH
import os
import importlib
import importlib.util
import logging
import func_timeout
os.chdir(POC_PATH)


# 安装poc的第三方组件
def poc2pip(require):
    cmd = 'pip install ' + require.replace('\r\n', ' ')
    res = os.system(cmd)
    return res


# 将poc写入指定目录
def poc2file(poc, key, require):
    if require:
        res = poc2pip(require)
        if not res == 0:
            return False
    poc_filename = str(key) + '.py'
    try:
        with open(poc_filename, 'w') as f:
            f.write(poc)
        return True
    except:
        return False


# 检测poc是否能正常执行
def poc_check(poc_item, host_item):
    # 动态加载poc并执行
    return True


if __name__ == '__main__':
    poc_list = 'poc1'
    host_list = '192.168.1.1'
    poc_check(poc_list, host_list)
