# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : pocfuns.py
from .. import models


def check_poc_permission(poc_id, user_get):
    if user_get.is_superuser:
        item_get = models.POC.objects.filter(id=poc_id).first()
    else:
        item_get = models.POC.objects.filter(user=user_get, id=poc_id).first()
    return item_get
