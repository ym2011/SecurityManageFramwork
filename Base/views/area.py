# coding:utf-8
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from django.views.decorators.csrf import csrf_protect


@api_view(['GET'])
def area_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        list_get = models.Area.objects.filter(name__icontains=key).order_by('id')
        list_count = list_get.count()
        serializers_get = serializers.AreaSerializer(instance=list_get, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['GET'])
def area_delete(request, area_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = models.Area.objects.filter(id=area_id).first()
        if item_get:
            item_get.delete()
            data['code'] = 0
            data['msg'] = '操作成功'
        else:
            data['msg'] = '你要干啥'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def area_create(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        form = forms.AreaForm(request.POST)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = '添加成功'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def area_update(request, area_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = models.Area.objects.filter(id=area_id).first()
        if item_get:
            form = forms.AreaForm(request.POST, instance=item_get)
            if form.is_valid():
                form.save()
                data['code'] = 0
                data['msg'] = '添加成功'
            else:
                data['msg'] = '请检查参数'
        else:
            data['msg'] = '数据错误'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)

