# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : client.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from django.db.models import Q
from django.views.decorators.csrf import csrf_protect


@api_view(['GET'])
def client_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key')
    if not key:
        key = ''
    list_get = models.Client.objects.filter(Q(ip__icontains=key)|Q(mac__icontains=key)).order_by('updatetime')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.ClientSerializer(instance=list_page, many=True)
    data['code'] = 0
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def client_delete(request, client_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    item_get = models.Client.objects.filter(id=client_id).first()
    if item_get:
        item_get.delete()
        data['code'] = 0
        data['msg'] = '操作成功'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def client_create(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    form = forms.ClientForm(request.POST)
    if form.is_valid():
        form.save()
        data['code'] = 0
        data['msg'] = '添加成功'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def client_update(request, client_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    client_get = models.Client.objects.filter(id=client_id).first()
    if client_get:
        form = forms.ClientForm(request.POST, instance=client_get)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = '添加成功'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '指定参数不存在'
    return JsonResponse(data)