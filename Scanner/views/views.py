# coding:utf-8
from .. import models, serializers
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter

# Create your views here.


@api_view(['GET'])
def main_list(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.Scanner.objects.filter(status=True).order_by('-updatetime')
    list_count = list_get.count()
    serializers_get = serializers.ScannerSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def select_list(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.Scanner.objects.filter(status=True).order_by('-updatetime')
    list_count = list_get.count()
    serializers_get = serializers.ScannerSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def police_list(request, scanner_id):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.Policies.objects.filter(scanner__id=scanner_id).order_by('-id')
    list_count = list_get.count()
    serializers_get = serializers.PoliceSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)

