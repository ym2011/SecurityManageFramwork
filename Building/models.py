# coding:utf-8
from django.db import models
from django.contrib.auth.models import User
from Base import  models as basemodels


# Create your models here.
class Building_Projects_LEVEL(models.Model):
    name = models.CharField('项目等级', max_length=100)
    descriptions = models.TextField('等级描述')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Building_Projects_LEVEL'
        verbose_name_plural = '项目等级'


class Building_Projects(models.Model):
    name = models.CharField('项目名称', max_length=30)
    person = models.ForeignKey(basemodels.Person, verbose_name='项目接口人', related_name='person_for_project', null=True, blank=True,
                              on_delete=models.SET_NULL)
    description = models.TextField('项目说明', null=True, blank=True)

    level = models.ForeignKey(Building_Projects_LEVEL, verbose_name='项目分级', related_name='level_for_project', null=True,
                              on_delete=models.SET_NULL)

    user = models.ForeignKey(User, related_name='user_for_projects', verbose_name='负责人',
                             on_delete=models.CASCADE)

    startdate = models.DateField('开始日期')
    enddate = models.DateField('计划结束日期')

    createtime = models.DateTimeField('添加时间', auto_now_add=True)
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Building_Projects'
        verbose_name_plural = '项目管理'


class Building_Projects_Action(models.Model):
    project = models.ForeignKey(Building_Projects, related_name='action_for_projects', verbose_name='项目', blank=True,
                             on_delete=models.CASCADE)
    person = models.ForeignKey(basemodels.Person, related_name='person_to_projectaction', null=True,blank=True , verbose_name='执行人',
                               on_delete=models.SET_NULL)
    action = models.TextField('进度说明', null=True, blank=True)
    actiontime = models.DateTimeField('进度日期')

    updatetime = models.DateTimeField('更新时间', auto_now_add=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Building_Projects_Action'
        verbose_name_plural = '项目进度'


class Building_Phishing(models.Model):
    name = models.CharField('钓鱼计划', max_length=100)
    key = models.CharField('鉴权key', max_length=100)
    number = models.IntegerField('发送基数',default=0)
    is_lock = models.BooleanField('是否锁定', default=False)
    scope = models.TextField('钓鱼范围')
    description = models.TextField('本次钓鱼测试说明')

    createtime = models.DateTimeField('创建时间', auto_now_add=True)
    updatetime = models.DateTimeField('操作时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Building_Phishing'
        verbose_name_plural = '钓鱼管理'


class Building_Phishing_Victim(models.Model):
    phishing = models.ForeignKey(Building_Phishing, related_name='phishing_for_victim', verbose_name='钓鱼计划', blank=True,
                                 on_delete=models.CASCADE)
    person = models.ForeignKey(basemodels.Person, related_name='person_for_victim', verbose_name='受害者',
                               on_delete=models.CASCADE)
    description = models.TextField('被钓鱼说明')

    createtime = models.DateTimeField('创建时间', auto_now_add=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Building_Phishing_Victim'
        verbose_name_plural = '受害者'



class Building_Event_LEVEL(models.Model):
    name = models.CharField('事件等级', max_length=100)
    descriptions = models.TextField('等级描述')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Building_Event_LEVEL'
        verbose_name_plural = '事件等级'


class Building_Event(models.Model):
    name = models.CharField('事件名称', max_length=30)
    person = models.ForeignKey(basemodels.Person, related_name='person_to_event',null=True ,blank=True ,verbose_name='责任人',on_delete=models.SET_NULL)
    description = models.TextField('背景说明', null=True, blank=True)
    scope = models.TextField('影响范围', null=True, blank=True)
    lose = models.TextField('事件损失', null=True, blank=True)

    level = models.ForeignKey(Building_Event_LEVEL, verbose_name='事件分级', related_name='level_for_event', null=True,
                              on_delete=models.SET_NULL)

    user = models.ForeignKey(User, related_name='user_for_event', verbose_name='跟进人员',
                             on_delete=models.CASCADE)

    startdate = models.DateField('开始日期')
    enddate = models.DateField('结束日期')

    starttime = models.DateTimeField('添加时间', auto_now_add=True)
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Building_Event'
        verbose_name_plural = '项目管理'


class Building_Event_Action(models.Model):
    event = models.ForeignKey(Building_Event, related_name='action_for_event', verbose_name='事件', on_delete=models.CASCADE)
    person = models.ForeignKey(basemodels.Person, related_name='person_to_eventaction', null=True, blank=True , verbose_name='责任人', on_delete=models.SET_NULL)
    action = models.TextField('进度说明', null=True, blank=True)

    actiontime = models.DateTimeField('进度日期')

    updatetime = models.DateTimeField('更新时间', auto_now_add=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Building_Event_Action'
        verbose_name_plural = '事件经纬'